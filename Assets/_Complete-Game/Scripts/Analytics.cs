﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Analytics;
using SimpleJSON;

public class Analytics: MonoBehaviour {
	
	private static Analytics instance = null;

	private const string version = "1.1";

	void Awake() {

		if (instance == null) {
			instance = this;
			InitializeUUID();
			DontDestroyOnLoad(gameObject);
		} else if (instance != this) {
			Destroy(gameObject);
		}

	}

	private string uuid;

	private void InitializeUUID() {

		string savedUUID = PlayerPrefs.GetString("analytics_uuid");

		Guid guid = new Guid();
		bool hasGuid = false;

		try { guid = new Guid(savedUUID); hasGuid = true; }
		catch (Exception e) { hasGuid = false; }

		if (!hasGuid) { guid = Guid.NewGuid(); }

		uuid = guid.ToString().ToUpper();

		if (savedUUID != uuid) {
			PlayerPrefs.SetString("analytics_uuid", uuid);
			PlayerPrefs.Save();
		}

	}

	private int currentLevel;
	private float currentLevelStartTime;
	private float firstLevelStartTime;

	private Counter moves;
	private Counter zombieHits;
	private Counter wallHits;
	private Counter wallDestructions;
	private Counter foodPickups;
	private Counter sodaPickups;

	public static void ReportLevelStart(int level, bool isFirstLevel) {

		instance.currentLevel = level;
		instance.currentLevelStartTime = Time.time;
		if (isFirstLevel) {
			instance.firstLevelStartTime = Time.time;
			instance.moves = new Counter();
			instance.zombieHits = new Counter();
			instance.wallHits = new Counter();
			instance.wallDestructions = new Counter();
			instance.foodPickups = new Counter();
			instance.sodaPickups = new Counter();
		}

		SendEvent("levelStart", new Dictionary<string, object> {
			{ "level", level }
		});

	}

	public static void NotifyMove() { instance.moves.Increment(); }
	public static void NotifyZombieHit() { instance.zombieHits.Increment(); }
	public static void NotifyWallHit() { instance.wallHits.Increment(); }
	public static void NotifyWallDestruction() { instance.wallDestructions.Increment(); }
	public static void NotifyFoodPickup() { instance.foodPickups.Increment(); }
	public static void NotifySodaPickup() { instance.sodaPickups.Increment(); }

	public static void ReportLevelSuccess(int remainigFood) {

		float levelTime = Time.time - instance.currentLevelStartTime;
		float totalTime = Time.time - instance.firstLevelStartTime;

		SendEvent("levelSuccess", new Dictionary<string, object> {
			{ "level", instance.currentLevel },
			{ "remainigFood", remainigFood },
			{ "levelTime", levelTime },
			{ "totalTime", totalTime },
			{ "levelMoves", instance.moves.current },
			{ "totalMoves", instance.moves.total },
			{ "levelZombieHits", instance.zombieHits.current },
			{ "totalZombieHits", instance.zombieHits.total },
			{ "levelWallHits", instance.wallHits.current },
			{ "totalWallHits", instance.wallHits.total },
			{ "levelWallDestructions", instance.wallDestructions.current },
			{ "totalWallDestructions", instance.wallDestructions.total },
			{ "levelFoodPickups", instance.foodPickups.current },
			{ "totalFoodPickups", instance.foodPickups.total },
			{ "levelSodaPickups", instance.sodaPickups.current },
			{ "totalSodaPickups", instance.sodaPickups.total }
		});

		instance.moves.NextLevel();
		instance.zombieHits.NextLevel();
		instance.wallHits.NextLevel();
		instance.wallDestructions.NextLevel();
		instance.foodPickups.NextLevel();
		instance.sodaPickups.NextLevel();

	}

	public static void ReportLevelFailure() {

		float levelTime = Time.time - instance.currentLevelStartTime;
		float totalTime = Time.time - instance.firstLevelStartTime;

		SendEvent("levelFailure", new Dictionary<string, object> {
			{ "level", instance.currentLevel },
			{ "levelTime", levelTime },
			{ "totalTime", totalTime },
			{ "levelMoves", instance.moves.current },
			{ "totalMoves", instance.moves.total },
			{ "levelZombieHits", instance.zombieHits.current },
			{ "totalZombieHits", instance.zombieHits.total },
			{ "levelWallHits", instance.wallHits.current },
			{ "totalWallHits", instance.wallHits.total },
			{ "levelWallDestructions", instance.wallDestructions.current },
			{ "totalWallDestructions", instance.wallDestructions.total },
			{ "levelFoodPickups", instance.foodPickups.current },
			{ "totalFoodPickups", instance.foodPickups.total },
			{ "levelSodaPickups", instance.sodaPickups.current },
			{ "totalSodaPickups", instance.sodaPickups.total }
		});

	}

	public static void ReportFoodPickup(string foodType, int newFoodCount) {
		
		float levelTime = Time.time - instance.currentLevelStartTime;

		SendEvent("pickup", new Dictionary<string, object> {
			{ "level", instance.currentLevel },
			{ "levelTimeSoFar", levelTime },
			{ "newFoodCount", newFoodCount },
			{ "foodType", foodType }
		});

	}

	public static void ReportTakeDamage(int remainigFood) {

		float levelTime = Time.time - instance.currentLevelStartTime;

		SendEvent("takeDamage", new Dictionary<string, object> {
			{ "level", instance.currentLevel },
			{ "levelTimeSoFar", levelTime },
			{ "remainigFood", remainigFood }
		});

	}

	// send to unity analytics and to the private server
	private static void SendEvent(string eventName, Dictionary<string, object> eventData) {

		eventData.Add("version", version);
		UnityEngine.Analytics.Analytics.CustomEvent(eventName, eventData);
		instance.SendToPrivateServer(eventName, eventData);

	}

	private void SendToPrivateServer(string eventName, Dictionary<string, object> eventData) {

		WWWForm postData = new WWWForm();

		postData.AddField("user_uuid", uuid);

		string timestamp = DateTime.UtcNow.ToString("u");
		postData.AddField("event_timestamp", timestamp);

		JSONObject data = new JSONObject();

		foreach (KeyValuePair<string, object> entry in eventData) {

			object valueObject = entry.Value;
			JSONNode node = null;

			if (valueObject is bool) { node = new JSONBool((bool)valueObject); }
			else if (valueObject is int) { node = new JSONNumber(valueObject.ToString()); }
			else if (valueObject is float) { node = new JSONNumber((float)valueObject); }
			else if (valueObject is string) { node = new JSONString((string)valueObject); }
			else { Debug.LogError("Unsupported type in analytics."); }

			if (node != null) { data.Add(entry.Key, node); }

		}

		postData.AddField("event_name", eventName);
		postData.AddField("event_data", data.ToString());

		WWW httpRequest = new WWW("https://thejv.club/roguelike-analytics/", postData);

		StartCoroutine(SendRequest(httpRequest));

	}

	private IEnumerator SendRequest(WWW httpRequest) {
		yield return httpRequest;
	}

}

public class Counter {

	public int total = 0;
	public int current = 0;

	public void Increment() {
		total += 1;
		current += 1;
	}

	public void NextLevel() {
		current = 0;
	}

}
